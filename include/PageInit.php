<?php
require_once dirname(__FILE__).'/Page.php';
class PageInit extends Page {
	function __construct($app_name=APP_NAME) {
		parent::__construct($app_name);
	}
	function js_main() {
		$js_main = <<< JSMAIN
function main() {
	insertmsg('Login Page');
	my_timer = setInterval("removemsg()", 10000);
}
JSMAIN;
		return $js_main;
	}
	function build_self() {
		$span = new HTMLObject('span');
		$span->insert_keyvalue('class','right');
		$span->do_multiline();
		$link = new HTMLObject('a');
		$link->insert_keyvalue('href','work.php?do=login');
		$link->insert_inner("Login");
		$span->append_object($link);
		$this->_domenu->append_object($span);
	}
	function build_page() {
		parent::build_page();
	}
}
?>
