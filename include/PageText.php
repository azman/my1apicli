<?php
require_once dirname(__FILE__).'/Page.php';
class PageText extends Page {
	protected $_showtext;
	protected $_initback;
	function __construct($app_name=APP_NAME,$text="Show This!",$back=false) {
		parent::__construct($app_name);
		$this->set_text($text);
		if ($back)
			$this->_initback = "\nsetInterval(\"history.back()\",10000);";
		else
			$this->_initback = "";
	}
	function set_text($text) {
		$this->_showtext = addslashes(trim($text));
	}
	function js_main() {
		$js_main = <<< JSMAIN
function main() {
	insertmsg("$this->_showtext");$this->_initback
}
JSMAIN;
		return $js_main;
	}
	function build_page() {
		parent::build_page();
	}
}
?>
