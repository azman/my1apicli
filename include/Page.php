<?php
require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/HTMLDocument.php';
class Page extends HTMLDocument {
	protected $_domenu;
	protected $_doview;
	protected $_dodata;
	function __construct($app_name=APP_NAME) {
		parent::__construct($app_name,true); // insert reset css
		$this->_domenu = null;
		$this->_doview = null;
		$this->_dodata = null;
	}
	function css_local() {
		$css_local = <<<CSSLOCAL
html, body { height: 100%; width: 100%; margin: 0; padding:0;
	overflow-x: hidden; font-family: Arial, sans-serif; }
div#site_menu { position: fixed; width: 100%; top: 0; height: 40px;
	line-height: 40px; z-index: 100; background-color: #0000FF; }
div#site_main { position: fixed; padding: 0; padding-top: 40px; }
div#site_view { position: fixed; padding: 0; width: 100%;
	background-color: #FF0000; }
div#site_foot { position: fixed; width: 100%; bottom: 0; z-index: 99;
	font-family:Courier; color: #444444; text-align: right; }
div#site_foot span.left { float: left; margin: 0; padding: 0;
	overflow: hidden; }
div#site_foot span.right { float: right; margin: 0; padding: 0;
	overflow: hidden; color: #CCCCCC; }
div#view_data { position: relative; padding: 0; }
div.view_hide { display: none; }
div#site_menu ul { list-style-type: none; }
div#site_menu li { float: left; }
div#site_menu a:link, div#site_menu a:visited { display: block; width: 120px;
	font-weight: bold; color: #FFFFFF; background-color: #0000FF;
	text-align: center; text-decoration: none; text-transform: uppercase; }
div#site_menu a:hover, div#site_menu a:active { background-color: #000080; }
div#site_menu span.left { float: left; margin: 0; padding: 0;
	overflow: hidden; }
div#site_menu span.right { float: right; margin: 0; padding: 0;
	overflow: hidden; }
div#site_menu ul li ul { display: none; position: absolute;
	top: 40px; width:120px; }
div#site_menu ul li:hover ul { display:block; }
div#site_menu ul li ul li a:link, div#site_menu ul li ul li a:visited {
	display: block; width: 120px; font-weight: normal; color: #FFFFFF;
	background-color: #0000FF; text-align: center; text-decoration: none;
	text-transform: none; }
div#site_menu ul li ul li a:hover, div#site_menu ul li ul li a:active {
	background-color: #C0C0C0; }
CSSLOCAL;
		return $css_local;
	}
	function js_local() {
		$js_local = <<< JSLOCAL
var my_timer = null;
function insertmsg(message) {
	var msg_view = document.getElementById('time_view');
	msg_view.innerHTML = '<span style="color:blue">'+message+'</span>';
}
function removemsg() {
	var msg_view = document.getElementById('time_view');
	msg_view.innerHTML = "";
	window.clearInterval(my_timer);
	my_timer = null;
}
// Cookies Load/Save - source: http://www.quirksmode.org/js/cookies.html
function createCookie(name,value,days) {
	name = 'my1_'+name;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	name = 'my1_'+name;
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
function removeCookie(name) {
	createCookie(name,"",-1);
}
JSLOCAL;
		return $js_local;
	}
	function js_main() {
		$appName = APP_NAME;
		$js_main = <<< JSMAIN
function main() {
	insertmsg('Welcome to $appName');
	my_timer = setInterval("removemsg()", 10000);
}
JSMAIN;
		return $js_main;
	}
	function build_main() {
		// site_main
		$main = new HTMLObject('div');
		$main->insert_id('site_main');
		$main->do_multiline();
		$this->append_2body($main);
		// site_view
		$view = new HTMLObject('div');
		$view->insert_id('site_view');
		$view->do_multiline();
		$main->append_object($view);
		$this->_doview = $view;
		// view_data
		$data = new HTMLObject('div');
		$data->insert_id('view_data');
		$data->do_multiline();
		$view->append_object($data);
		$this->_dodata = $data;
	}
	function build_menu() {
		$menu = new HTMLObject('div');
		$menu->insert_id('site_menu');
		$menu->do_multiline();
		$this->append_2body($menu);
		$this->_domenu = $menu;
	}
	function build_self() {
		// override this in child!
		//$this->_doview
		//$this->_dodata
	}
	function build_foot() {
		$foot = new HTMLObject('div');
		$foot->insert_id('site_foot');
		$foot->do_multiline();
		$this->append_2body($foot);
		// use this as dynamic message spot - using timeout to remove!
		$spot = new HTMLObject('span');
		$spot->insert_id('time_view');
		$spot->insert_keyvalue('class','left');
		$spot->do_1skipline();
		$foot->append_object($spot);
		// my mark!
		$mark = new HTMLObject('span');
		$mark->insert_keyvalue('class','right');
		$mark->insert_inner('azman@my1matrix.org');
		$mark->do_1skipline();
		$foot->append_object($mark);
	}
	function build_page() {
		// local style
		$temp = new CSSObject('css_local');
		$temp->insert_inner($this->css_local());
		$this->append_2head($temp);
		// local script
		$temp = new JSObject('js_local');
		$temp->insert_inner($this->js_local());
		$this->append_2head($temp);
		// main script - child should override js_main
		$temp = new JSObject('js_main');
		$temp->insert_inner($this->js_main());
		$this->append_2body($temp);
		// load main() on page load
		$this->insert_onload('main()');
		// build main site
		$this->build_main();
		// build menu
		$this->build_menu();
		// build custom
		$this->build_self();
		// build footer
		$this->build_foot();
	}
	function write_html() {
		$this->build_page();
		parent::write_html();
	}
	function Show() {
		$this->write_html();
		exit();
	}
	protected function throw_debug($error) {
		throw new Exception('['.get_class($this).'] => {'.$error.'}');
	}
}
?>
