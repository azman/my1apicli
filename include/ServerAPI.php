<?php
class ServerAPI {
	private $_app_key;
	private $_api_url;
	public function __construct($apikey,$apisrv) {
		$this->_app_key = $apikey;
		$this->_api_url = $apisrv;
	}
	public function request($method,$target,$params,$secure=false) {
		$headers = array();
		if ($secure) {
			$headers[] = 'Accept: application/text';
			$headers[] = 'Content-Type: application/text';
			$paramz = $this->encode($params);
		} else {
			$headers[] = 'Accept: application/json';
			$headers[] = 'Content-Type: application/json';
			$paramz = json_encode($params);
		}
		$apicall = curl_init();
		$curl_target = $this->_api_url."".$target;
		curl_setopt($apicall,CURLOPT_URL,$curl_target);
		curl_setopt($apicall,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($apicall,CURLOPT_CUSTOMREQUEST,$method);
		curl_setopt($apicall,CURLOPT_POSTFIELDS,$paramz);
		curl_setopt($apicall,CURLOPT_HTTPHEADER,$headers);
		$output = curl_exec($apicall);
		if ($secure) {
			$result['data'] = $this->decode($output);
			$result['flag'] = true;
		} else $result = json_decode($output,true);
		return $result;
	}
	public function encode($params) {
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128,
			$this->_app_key, json_encode($params), MCRYPT_MODE_ECB));
	}
	public function decode($params) {
		return json_decode(trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,
			$this->_app_key, base64_decode($params), MCRYPT_MODE_ECB)),true);
	}
}
?>
