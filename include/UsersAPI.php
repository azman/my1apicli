<?php
require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/ServerAPI.php';
class UsersAPI extends ServerAPI {
	protected $_api_user;
	protected $_api_pass;
	protected $_newbies_;
	protected $_isAdmin_;
	protected $_isGuest_;
	protected $_goLevel_;
	protected $_goChkID_;
	protected $_chkName_;
	public function __construct($api_user,$api_pass) {
		parent::__construct(APP_MY1KEY,API_SERVER);
		$this->_api_user = null;
		$this->_api_pass = null;
		$this->_newbies_ = true;
		$this->_isAdmin_ = false;
		$this->_isGuest_ = true;
		$this->_goLevel_ = -1;
		$this->_goChkID_ = 0;
		$this->_chkName_ = "Guest";
		if (!empty($api_user)&&!empty($api_pass)) {
			$this->_api_user = $api_user;
			$this->_api_pass = $api_pass;
			$result = $this->request("POST","users/login",array());
			if (isset($result['flag'])&&($result['flag']==true)) {
				$data = $result['data'];
				if (array_key_exists('login',$data)&&($data['login']=="okay")) {
					$this->_isGuest_ = false;
					if ($data['level']==0) $this->_isAdmin_ = true;
					$this->_goLevel_ = intval($data['level']);
					$this->_goChkID_ = intval($data['chkid']);
					$this->_chkName_ = $api_user;
				}
			}
			$this->_newbies_ = false;
		}
	}
	public function request($method,$target,$params) {
		$params['username'] = $this->_api_user;
		$params['userpass'] = $this->_api_pass;
		return parent::request($method,$target,$params,API_SECURE);
	}
	public function isFirst() {
		return $this->_newbies_;
	}
	public function isAdmin() {
		return $this->_isAdmin_;
	}
	public function isGuest() {
		return $this->_isGuest_;
	}
	public function goLevel() {
		return $this->_goLevel_;
	}
	public function goChkID() {
		return $this->_goChkID_;
	}
	public function chkName() {
		return $this->_chkName_;
	}
	public function serve_user_request($ureqpack) {
		if ($this->isGuest()) return null;
		$task = $ureqpack['urequest'];
		$site = $ureqpack['ureqsite'];
		$data = $ureqpack['ureqdata'];
		if(!isset($ureqpack['ureqload'])) $ureqpack['ureqload'] = false;
		if ($data['user']==$this->goChkID()) {
			$site = $site.$data['user'];
			// reload if deleting myself
			if ($task=="DELETE") $ureqpack['ureqload'] = true;
		}
		$ureq = $this->request($task,$site,$data);
		//throw new Exception('[DEBUG] '.json_encode($ureq));
		if (isset($ureq['flag'])&&$ureq['flag']==true) {
			if ($ureqpack['ureqload']) {
				header('Location: logout.php');
				exit();
			}
			$ureqdone = $ureq;
		} else {
			$ureqdone = false;
		}
		return $ureqdone;
	}
}
?>
