<?php
require_once dirname(__FILE__).'/Page.php';
class PageBase extends Page {
	protected $_dobase;
	protected $_server;
	protected $_dosave;
	protected $_dolist;
	function __construct($app_name=APP_NAME,$server,$dosave) {
		parent::__construct($app_name);
		if (isset($this->_dobase)&&$this->_dobase!=true) {
			$this->_server = null;
			$this->_dosave = null;
		} else {
			$this->_dobase = true;
			//if (!is_a($server,"ServerAPI")) {
			if (! $server instanceof ServerAPI) {
				$this->throw_debug('Invalid API client object!');
			}
			$this->_server = $server;
			$this->_dosave = $dosave;
			$this->_dolist = $this->server_list_user();
		}
	}
	function server_list_user() {
		$task = 'GET'; $site = 'users/';
		if (!$this->_server->isAdmin())
			$site = $site.$this->_server->goChkID();
		$list = $this->_server->request($task,$site,array());
		if (isset($list['flag'])&&$list['flag']==true)
			return $list['data'];
		else return null;
	}
	function build_page() {
		parent::build_page();
	}
}
?>
