<?php
require_once dirname(__FILE__).'/PageLogin.php';
class PageUser extends PageLogin {
	protected $_task;
	protected $_done;
	function __construct($app_name=APP_NAME,$server,$dosave,$task,$done) {
		$this->_dobase = true;
		parent::__construct($app_name,$server,$dosave);
		$this->_task = $task;
		$this->_done = $done;
	}
	function css_more() {
		$css_more = <<<CSSMORE
div#user_form { position: absolute; top: 0; width: 100%; margin: 0 auto;
	visibility: hidden; }
form.form_view { background-color: #f3f3f3; margin: 0 auto;
	margin-top: 40px; border: solid 1px #a1a1a1; padding: 10px; width: 400px; }
form.form_view label, input { display: block; width: 180px;
	float: left; margin-bottom: 10px; }
form.form_view label { text-align: right; padding-right: 20px; }
form.form_view br {  clear: left; }
div.view_init { position: relative; padding: 10px; border: 2px solid #000080;
	color: #0000ff; font-family: "Verdana"; text-align: center; }
CSSMORE;
		return $css_more;
	}
	function js_main() {
		if ($this->_server->isGuest()) {
			$this->throw_debug('Invalid login?!');
		}
		$jsvar_main_start = "";
		if ($this->_dolist!=null) {
			$jsvar_main_start = "\tdata_copy = JSON.parse('".
				json_encode($this->_dolist,true)."');\n".
				"\tself_id = \"".$this->_server->goChkID()."\";\n";
		}
		$jsvar_iflag = $this->_server->isAdmin() ? "false" : "true";
		$js_main = <<< JSMAIN
var data_copy = null;
var this_id = null;
var self_id = null;
var this_ix = null;
var self_ix = null;
function main() {
$jsvar_main_start	// link nodes
	var that_id = readCookie('this_id');
	var checkme = false;
	if (data_copy!=null) {
		for (var loop=0;loop<data_copy.count;loop++) {
			var dhash = 'user'+loop;
			var ddata = data_copy[dhash];
			// find id based on cookie
			if (ddata.id==that_id) {
				this_id = that_id;
				this_ix = dhash;
			}
			if (ddata.id==self_id)
			{
				checkme = true;
				self_ix = dhash;
			}
		}
	}
	if (checkme==true&&this_id!=null)
		show_form('$this->_task',$this->_done);
	else
		alert('Invalid ID!');
}
function show_form(option,update) {
	var that_ix = this_ix;
	var formInfo = document.getElementById('form_info');
	if (formInfo) {
		formInfo.reset();
		if (update==2) that_ix = self_ix;
		var buttInfo = document.getElementById('butt_info');
		if (data_copy) {
			test = data_copy[that_ix];
			if (update) {
				// update current value
				formInfo.user.value = test.id;
				formInfo.name.value = test.name;
				formInfo.full.value = test.fname;
				formInfo.mail.value = test.email;
				switch (test.flag) {
					case 0: case 1: case 2:
						formInfo.flag.value = test.flag;
						break;
					default: formInfo.flag.value = 2; break;
				}
			}
		}
		// which link did the user hit?
		if (option=="modify"&&test!=null) {
			if (that_ix!=self_ix&&
					data_copy[that_ix].flag<=data_copy[self_ix].flag) {
				alert('Not privileged!');
				history.back(); // do not show the form!
			}
			formInfo.name.disabled = false;
			formInfo.full.disabled = false;
			formInfo.mail.disabled = false;
			formInfo.flag.disabled = $jsvar_iflag;
			formInfo.pass.disabled = true;
			formInfo.pchk.disabled = true;
			buttInfo.innerHTML = "Update";
		} else if (option=="delete"&&test!=null) {
			if (that_ix!=self_ix&&
					data_copy[that_ix].flag<=data_copy[self_ix].flag) {
				alert('Not privileged!');
				history.back(); // do not show the form!
			}
			formInfo.name.disabled = true;
			formInfo.full.disabled = true;
			formInfo.mail.disabled = true;
			formInfo.flag.disabled = true;
			formInfo.pass.disabled = true;
			formInfo.pchk.disabled = true;
			buttInfo.innerHTML = "Delete";
		} else if (option=="dopass"&&test!=null) {
			formInfo.name.disabled = true;
			formInfo.full.disabled = true;
			formInfo.mail.disabled = true;
			formInfo.flag.disabled = true;
			formInfo.pass.disabled = false;
			formInfo.pchk.disabled = false;
			buttInfo.innerHTML = "Change Password";
		} else if (option=="create") {
			formInfo.name.disabled = false;
			formInfo.full.disabled = false;
			formInfo.mail.disabled = false;
			formInfo.flag.disabled = false;
			formInfo.pass.disabled = false;
			formInfo.pchk.disabled = false;
			buttInfo.innerHTML = "Create";
		} else {
			history.back(); // do not show the form!
		}
		// show me the money!??? ... form!
		document.getElementById('user_form').style.visibility = "visible";
	}
}
function send_form() {
	var formInfo = document.getElementById('form_info');
	if (formInfo) {
		var buttInfo = document.getElementById('butt_info');
		var test = data_copy[this_ix];
		if (buttInfo.innerHTML=="Update") {
			var chkone = false;
			if (formInfo.name.value == test.name) {
				formInfo.name.disabled = true;
			} else chkone = true;
			if (formInfo.full.value == test.fname) {
				formInfo.full.disabled = true;
			} else chkone = true;
			if (formInfo.mail.value == test.email) {
				formInfo.mail.disabled = true;
			} else chkone = true;
			if (formInfo.flag.value == test.flag) {
				formInfo.flag.disabled = true;
			} else chkone = true;
			if (chkone===false) {
				alert('Nothing to update!');
				return false;
			}
			formInfo.action="work.php?do=usermod";
		} else if (buttInfo.innerHTML=="Delete") {
			formInfo.name.disabled = false;
			formInfo.mail.disabled = false;
			formInfo.action="work.php?do=userdel";
		} else if (buttInfo.innerHTML=="Change Password") {
			if (formInfo.pchk.value!=formInfo.pass.value) {
				alert('Password mismatch!');
				return false;
			} else if(formInfo.pass.value=="") {
				alert('Nothing to update!');
				return false;
			} else {
				formInfo.pass.value = sha512(formInfo.pass.value);
				formInfo.name.disabled = false;
				formInfo.action="work.php?do=userpwd";
				formInfo.submit();
			}
		} else if (buttInfo.innerHTML=="Create") {
			if (formInfo.pchk.value!=formInfo.pass.value) {
				alert('Password mismatch!');
				return false;
			}
			formInfo.pass.value = sha512(formInfo.pass.value);
			formInfo.action="work.php?do=useradd";
		}
		formInfo.submit();
	}
}
JSMAIN;
		return $js_main;
	}
	function build_self() {
		// build user form
		$duser = new HTMLObject('div');
		$duser->insert_id('user_form');
		$duser->do_multiline();
		$this->_dodata->append_object($duser);
		// create header
		$dhead = new HTMLObject('div');
		$dhead->insert_keyvalue('class','view_init');
		$dhead->insert_inner('User Information');
		$dhead->insert_linebr();
		$dhead->do_1skipline();
		$duser->append_object($dhead);
		// create form
		$dform = new HTMLObject('form');
		$dform->insert_id('form_info');
		$dform->insert_keyvalue('method','POST');
		$dform->insert_keyvalue('class','form_view');
		$dform->insert_keyvalue('onkeypress',
			'jacascript:return event.keyCode != 13;');
		$dform->do_multiline();
		$duser->append_object($dform);
		// create username input
		$lname = new HTMLObject('label');
		$lname->insert_inner('Username');
		$lname->do_1skipline();
		$dform->append_object($lname);
		$uname = new HTMLObject('input');
		$uname->insert_keyvalue('type','text');
		$uname->insert_keyvalue('name','name');
		$uname->remove_tail();
		$uname->do_1skipline();
		$dform->append_object($uname);
		// create password input
		$lpass = new HTMLObject('label');
		$lpass->insert_inner('Password');
		$lpass->do_1skipline();
		$dform->append_object($lpass);
		$upass = new HTMLObject('input');
		$upass->insert_keyvalue('type','password');
		$upass->insert_keyvalue('name','pass');
		$upass->remove_tail();
		$upass->do_1skipline();
		$dform->append_object($upass);
		// create password repeat input
		$lpas2 = new HTMLObject('label');
		$lpas2->insert_inner('Password (again)');
		$lpas2->do_1skipline();
		$dform->append_object($lpas2);
		$upas2 = new HTMLObject('input');
		$upas2->insert_keyvalue('type','password');
		$upas2->insert_keyvalue('name','pchk');
		$upas2->remove_tail();
		$upas2->do_1skipline();
		$dform->append_object($upas2);
		// create fullname input
		$lfull = new HTMLObject('label');
		$lfull->insert_inner('Full Name');
		$lfull->do_1skipline();
		$dform->append_object($lfull);
		$ufull = new HTMLObject('input');
		$ufull->insert_keyvalue('type','text');
		$ufull->insert_keyvalue('name','full');
		$ufull->remove_tail();
		$ufull->do_1skipline();
		$dform->append_object($ufull);
		// create email input
		$lmail = new HTMLObject('label');
		$lmail->insert_inner('Email');
		$lmail->do_1skipline();
		$dform->append_object($lmail);
		$umail = new HTMLObject('input');
		$umail->insert_keyvalue('type','text');
		$umail->insert_keyvalue('name','mail');
		$umail->remove_tail();
		$umail->do_1skipline();
		$dform->append_object($umail);
		// create level input
		$lflag = new HTMLObject('label');
		$lflag->insert_inner('Level');
		$lflag->do_1skipline();
		$dform->append_object($lflag);
		$uflag = new HTMLObject('select');
		$uflag->insert_id('usrlevel');
		$uflag->insert_keyvalue('name','flag');
		$uflag->do_multiline();
		$uflag->insert_linebr(2);
		$dform->append_object($uflag);
		// create options for levels
		$doopt = new HTMLObject('option');
		$doopt->insert_keyvalue('value','2',true);
		$doopt->insert_inner('User');
		$doopt->do_1skipline();
		$uflag->append_object($doopt);
		$doopt = new HTMLObject('option');
		$doopt->insert_keyvalue('value','1',true);
		$doopt->insert_inner('Power User');
		$doopt->do_1skipline();
		$uflag->append_object($doopt);
		$doopt = new HTMLObject('option');
		$doopt->insert_keyvalue('value','0',true);
		$doopt->insert_inner('Administrator');
		$doopt->do_1skipline();
		$uflag->append_object($doopt);
		// hidden id stuff
		$luser = new HTMLObject('label');
		$luser->insert_inner('User ID');
		$luser->insert_keyvalue('style','display:none;');
		$luser->do_1skipline();
		$dform->append_object($luser);
		$uuser = new HTMLObject('input');
		$uuser->insert_keyvalue('type','text');
		$uuser->insert_keyvalue('name','user');
		$uuser->insert_keyvalue('style','display:none;');
		$uuser->remove_tail();
		$uuser->do_1skipline();
		$dform->append_object($uuser);
		// create buttons
		$ubutt = new HTMLObject('button');
		$ubutt->insert_id('butt_info');
		$ubutt->insert_keyvalue('onclick','javascript:send_form();');
		$ubutt->insert_inner('Update');
		$ubutt->do_1skipline();
		$dform->append_object($ubutt);
		$dlink = new HTMLObject('a');
		$dlink->insert_keyvalue('href','javascript:history.back();');
		$dlink->insert_inner('Cancel');
		$dform->append_object($dlink);
	}
	function build_page() {
		parent::build_page();
	}
}
?>
