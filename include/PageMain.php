<?php
require_once dirname(__FILE__).'/PageBase.php';
class PageMain extends PageBase {
	function __construct($app_name=APP_NAME,$server,$dosave) {
		parent::__construct($app_name,$server,$dosave);
	}
	function create_style_text($ctext,$cstyle) {
		if ($cstyle!="") $cstyle = " style=\"".$cstyle."\"";
		$ctext = "<span".$cstyle.">".$ctext."</span>";
		return $ctext;
	}
	function create_title_text($ctext,$title) {
		$ctext = "<b>".$title.": </b> ".$ctext;
		return $ctext;
	}
	function create_color_text($ctext,$color) {
		$ctext = "<span style=\"color:".$color."\">".$ctext."</span>";
		return $ctext;
	}
	function create_listlink($label,$hlink) {
		$list = new HTMLObject('li');
		$link = new HTMLObject('a');
		$link->insert_keyvalue('href',$hlink);
		$link->insert_inner($label);
		$list->insert_object($link);
		$list->do_1skipline();
		return $list;
	}
	function create_menu_sub($label,$items) {
		$menus = new HTMLObject('li');
		$menus->do_multiline();
		$menut = new HTMLObject('a');
		$menut->insert_keyvalue('href','');
		$menut->insert_inner($label);
		$menut->do_1skipline();
		$menus->append_object($menut);
		$menuc = new HTMLObject('ul');
		$menuc->do_multiline();
		foreach ($items as $item) {
			$menui = $this->create_listlink($item['label'],$item['hlink']);
			$menuc->append_object($menui);
		}
		$menus->append_object($menuc);
		return $menus;
	}
	function create_user_item($user) {
		$ustyle = "width:150px;";
		$dotemp = $this->create_style_text($user['name'],$ustyle);
		$output = $this->create_title_text($dotemp,"User")."\n";
		$ustyle = "width:120px;";
		$dotemp = "User";
		switch ($user['flag']) {
			default:
			case 2: $dotemp = "User"; break;
			case 1: $dotemp = "Power User"; break;
			case 0: $dotemp = "Administrator"; break;
		}
		$dotemp = $this->create_style_text($dotemp,$ustyle);
		$output = $output.$this->create_title_text($dotemp,"Role")."\n";
		$ustyle = "width:280px;";
		$dotemp = $this->create_style_text($user['fname'],$ustyle);
		$output = $output.$this->create_title_text($dotemp,"Full Name")."\n";
		$ustyle = "";
		$dotemp = $this->create_style_text($user['email'],$ustyle);
		$output = $output.$this->create_title_text($dotemp,"E-mail")."\n";
		return $output;
	}
	function css_more() {
		$css_more = <<<CSSMORE
div.view_node { position: relative; padding: 5px; width: 100%;
	text-align: left; background-color: #AAAAAA; display: inline-block; }
div.view_node span { display: inline-block; color: blue; }
CSSMORE;
		return $css_more;
	}
	function js_main() {
		$jsvar_main_message = "";
		$jsvar_main_start = "";
		if (!$this->_server->isFirst()) {
			if ($this->_server->isGuest()) {
				$jsvar_main_message =
					$this->create_color_text("Login Failed?!","red");
			} else {
				$jsvar_main_message =
					$this->create_color_text("Welcome to ".APP_NAME."!","blue");
				if ($this->_dolist!=null) {
					$jsvar_main_start = "\tdata_copy = JSON.parse('".
						json_encode($this->_dolist,true)."');\n".
						"\tself_id = \"".$this->_server->goChkID()."\";\n";
				}
				if ($this->_dosave!==null) {
					if ($this->_dosave===false) {
						$jsvar_main_message =
							$this->create_color_text("Request Rejected!",
								"red");
					} else {
						$ureqmesg = "Request Completed!";
						$ureqdone = $this->_dosave;
						if (isset($ureqdone['mesg'])) {
							$ureqmesg = $ureqdone['mesg'];
						}
						$ureqdone = $ureqdone['data'];
						$jsvar_main_start = $jsvar_main_start.
							"\tlast_id = ".$ureqdone['user'].";\n";
						$jsvar_main_message =
							$this->create_color_text($ureqmesg,"blue");
					}
					$this->_dosave = null;
				}
			}
		}
		$jsvar_iflag = $this->_server->isAdmin() ? "false" : "true";
		$js_main = <<< JSMAIN
var data_copy = null;
var this_node = null;
var self_node = null;
var this_id = null;
var self_id = null;
var last_id = null;
var bgnode_active = "#888888", bgnode_normal = "#AAAAAA";
function main() {
$jsvar_main_start	// link nodes
	if (data_copy!=null) {
		var fnode = null, lnode = null;
		for (var loop=0;loop<data_copy.count;loop++) {
			var dnode = document.getElementById('user'+loop);
			var ddata = data_copy[dnode.id];
			dnode.onclick = passthis(dnode); //activeNode;
			// make first node as active
			if (fnode==null) fnode = dnode;
			if (ddata.id==self_id) self_node = dnode;
			if (last_id!==null&&ddata.id===last_id) lnode = dnode;
		}
		if (lnode!==null) activeNode(lnode);
		else if (self_node!==null) activeNode(self_node);
		else if (fnode!==null) activeNode(fnode);
	}
	insertmsg('$jsvar_main_message');
	my_timer = setInterval("removemsg()", 10000);
}
function passthis(that) {
	return function() { activeNode(that); };
}
function activeNode(node) {
	if (this_node) {
		this_node.style.backgroundColor = bgnode_normal;
		removeCookie('this_id');
		this_id = null;
	}
	this_node = node;
	if (this_node) {
		this_id = data_copy[this_node.id].id;
		createCookie('this_id',this_id,1);
		this_node.style.backgroundColor = bgnode_active;
	}
}
JSMAIN;
		return $js_main;
	}
	function build_self() {
		// create user list
		$list = $this->_dolist;
		if ($list) {
			for($loop=0;$loop<$list['count'];$loop++) {
				$node = new HTMLObject('div');
				$node->insert_id('user'.$loop);
				$node->insert_keyvalue('class','view_node');
				$node->insert_inner(
					$this->create_user_item($list['user'.$loop]));
				$node->do_multiline();
				$this->_dodata->append_object($node);
			}
		}
		// create menu
		$span = new HTMLObject('span');
		$span->insert_keyvalue('class','right');
		$span->do_multiline();
		$this->_domenu->append_object($span);
		if (!$this->_server->isGuest()) {
			$menu_title = $this->_server->chkName();
			$menu_items = [
				[ 'label' => 'Create User',
					'hlink' => "work.php?do=user&task=create&done=0" ],
				[ 'label' => 'Modify User',
					'hlink' => "work.php?do=user&task=modify&done=1" ],
				[ 'label' => 'Remove User',
					'hlink' => "work.php?do=user&task=delete&done=1" ],
				[ 'label' => 'Change Pass',
					'hlink' => "work.php?do=user&task=dopass&done=2" ]
			];
			$menu_extra = [ 'label' => "Logout", 'hlink' => "logout.php" ];
			if (!$this->_server->isAdmin()) {
				array_shift($menu_items);
				$menu_items[0]['label'] = 'Modify Self';
				$menu_items[1]['label'] = 'Remove Self';
			}
			array_push($menu_items,$menu_extra);
			$menus = $this->create_menu_sub($menu_title,$menu_items);
			$menum = new HTMLObject('ul');
			$menum->do_multiline();
			$menum->append_object($menus);
			// can actually add more horiz items!
			$span->append_object($menum);
		} else {
			$link = new HTMLObject('a');
			$link->insert_keyvalue('href','work.php?do=login');
			$link->insert_inner("Login");
			$span->append_object($link);
		}
	}
	function build_page() {
		parent::build_page();
		// more styles
		$temp = new CSSObject('css_more');
		$temp->insert_inner($this->css_more());
		$this->append_2head($temp);
	}
}
?>
