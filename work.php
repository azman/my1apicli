<?php
require_once dirname(__FILE__).'/include/config.php';
try {
	if ($_SERVER['REQUEST_METHOD']=='POST') {
		if (array_key_exists('username',$_POST)&&
				array_key_exists('password',$_POST)) {
			$username = $_POST['username'];
			$userpass = $_POST['password'];
			session_start();
			$_SESSION['username'] = $username;
			$_SESSION['userpass'] = $userpass;
			header('Location: work.php');
			exit();
		} else {
			if (array_key_exists('do',$_GET)) {
				switch($_GET['do']) {
					case 'usermod':
						session_start();
						$ureqdata = array();
						$ureqdata['user'] = $_POST['user'];
						if (isset($_POST['name'])) {
							$ureqdata['name'] = $_POST['name'];
						}
						if (isset($_POST['flag'])) {
							$ureqdata['flag'] = $_POST['flag'];
						}
						if (isset($_POST['full'])) {
							$ureqdata['fname'] = $_POST['full'];
						}
						if (isset($_POST['mail'])) {
							$ureqdata['email'] = $_POST['mail'];
						}
						$ureqpack['urequest'] = "PUT";
						$ureqpack['ureqsite'] = "users/";
						$ureqpack['ureqdata'] = $ureqdata;
						$_SESSION['ureqpack'] = $ureqpack;
						header('Location: work.php');
						exit();
						break;
					case 'userdel':
						session_start();
						$ureqdata = array();
						$ureqdata['user'] = $_POST['user'];
						$ureqdata['name'] = $_POST['name'];
						$ureqdata['email'] = $_POST['mail'];
						$ureqpack['urequest'] = "DELETE";
						$ureqpack['ureqsite'] = "users/";
						$ureqpack['ureqdata'] = $ureqdata;
						$_SESSION['ureqpack'] = $ureqpack;
						header('Location: work.php');
						exit();
						break;
					case 'userpwd':
						session_start();
						$ureqdata = array();
						$ureqdata['user'] = $_POST['user'];
						$ureqdata['name'] = $_POST['name'];
						$ureqdata['pass'] = $_POST['pass'];
						$ureqpack['urequest'] = "PUT";
						$ureqpack['ureqsite'] = "users/";
						$ureqpack['ureqdata'] = $ureqdata;
						$ureqpack['ureqload'] = true;
						$_SESSION['ureqpack'] = $ureqpack;
						header('Location: work.php');
						exit();
						break;
					case 'useradd':
						session_start();
						$ureqdata = array();
						$ureqdata['name'] = $_POST['name'];
						$ureqdata['pass'] = $_POST['pass'];
						$ureqdata['flag'] = $_POST['flag'];
						$ureqdata['fname'] = $_POST['full'];
						$ureqdata['email'] = $_POST['mail'];
						$ureqpack['urequest'] = "POST";
						$ureqpack['ureqsite'] = "users/";
						$ureqpack['ureqdata'] = $ureqdata;
						$_SESSION['ureqpack'] = $ureqpack;
						header('Location: work.php');
						exit();
						break;
					default:
						throw new Exception('Invalid GetPost');
				}
			} else {
				throw new Exception('Invalid Post!');
			}
		}
	}
	session_start();
	if (!isset($_SESSION['username'])||!isset($_SESSION['userpass'])) {
		if (isset($_GET['do'])&&$_GET['do']=='login') {
			require_once dirname(__FILE__).'/include/PageLogin.php';
			$page = new PageLogin();
			$page->Show();
			//throw new Exception('PageLogin!');
		} else {
			session_destroy();
			require_once dirname(__FILE__).'/include/PageInit.php';
			$page = new PageInit();
			$page->Show();
			//throw new Exception('PageInit!');
		}
	}
	require_once dirname(__FILE__).'/include/UsersAPI.php';
	$server = new UsersAPI($_SESSION['username'],$_SESSION['userpass']);
	if ($server->isGuest()) {
		session_destroy();
		header('Location: work.php');
		exit();
	}
	//throw new Exception('[DEBUG] HERE!');
	$dosave = null;
	if (isset($_SESSION['ureqpack'])) {
		$chpasswd = false;
		$ureqpack = $_SESSION['ureqpack'];
		if ($ureqpack['urequest']=='PUT'&&$ureqpack['ureqload']==true) {
			$chpasswd = true;
			$ureqpack['ureqload']=false;
		}
		$dosave = $server->serve_user_request($ureqpack);
		if ($chpasswd==true&&$dosave!=false) {
			$ureqdata = $ureqpack['ureqdata'];
			if ($_SESSION['username']!=$ureqdata['name']) {
				throw new Exception('Something is WRONG!');
			}
			$_SESSION['userpass'] = $ureqdata['pass'];
			$dosave['mesg'] = "Password changed!";
			// recreate server object with new passwd?
			$server = new UsersAPI($_SESSION['username'],
				$_SESSION['userpass']);
			if ($server->isGuest()) {
				session_destroy();
				header('Location: work.php');
				exit();
			}
		}
		unset($_SESSION['ureqpack']);
	}
	if (isset($_GET['do'])) {
		if ($_GET['do']=='user') {
			if (!isset($_GET['task'])||!isset($_GET['done'])) {
				throw new Exception('Invalid user task!');
			}
			require_once dirname(__FILE__).'/include/PageUser.php';
			$page = new PageUser(APP_NAME,$server,$dosave,
				$_GET['task'],$_GET['done']);
			$page->Show();
		} else {
			throw new Exception('Invalid Client!');
		}
	} else {
		//throw new Exception('Main Page!');
		// do main page?
		require_once dirname(__FILE__).'/include/PageMain.php';
		$page = new PageMain(APP_NAME,$server,$dosave);
		$page->Show();
	}
} catch (Exception $error) {
	session_start();
	session_destroy();
	require_once dirname(__FILE__).'/include/PageText.php';
	$page = new PageText(APP_NAME,"[ERROR] ".$error->getMessage());
	$page->Show();
}
?>
